var CryptoJS = require('./crypto-js/crypto-js')
//单向加密 SHA1 SHA224 SHA256 SHA384 SHA512 MD5 HmacSHA1 HmacSHA224 HmacSHA256 HmacSHA384 HmacSHA512 HmacMD5 PBKDF2
function SHA1(message) {
    return CryptoJS.SHA1(message).toString()
}
function SHA224(message) {
    return CryptoJS.SHA224(message).toString()
}
function SHA256(message) {
    return CryptoJS.SHA256(message).toString()
}
function SHA384(message) {
    return CryptoJS.SHA384(message).toString()
}
function SHA512(message) {
    return CryptoJS.SHA512(message).toString()
}
function MD5(message) {
    return CryptoJS.MD5(message).toString()
}
function HmacSHA1(message,key) {
    return CryptoJS.HmacSHA1(message,key).toString()
}
function HmacSHA224(message,key) {
    return CryptoJS.HmacSHA224(message,key).toString()
}
function HmacSHA256(message,key) {
    return CryptoJS.HmacSHA256(message,key).toString()
}
function HmacSHA384(message,key) {
    return CryptoJS.HmacSHA384(message,key).toString()
}
function HmacMD5(message,key) {
    return CryptoJS.HmacMD5(message,key).toString()
}
function HmacSHA512(message,key) {
    return CryptoJS.HmacSHA512(message,key).toString()
}
function AES_ET(message,key) {
    return CryptoJS.AES.encrypt(message,key).toString()
}
function AES_DT(message,key) {
    return CryptoJS.AES.decrypt(message,key).toString(CryptoJS.enc.Utf8)
}
function DES_ET(message,key) {
    return CryptoJS.DES.encrypt(message,key).toString()
}
function DES_DT(message,key) {
    return CryptoJS.DES.decrypt(message,key).toString(CryptoJS.enc.Utf8)
}
function RC4_ET(message,key) {
    return CryptoJS.RC4.encrypt(message,key).toString()
}
function RC4_DT(message,key) {
    return CryptoJS.RC4.decrypt(message,key).toString(CryptoJS.enc.Utf8)
}
function Rabbit_ET(message,key) {
    return CryptoJS.Rabbit.encrypt(message,key).toString()
}
function Rabbit_DT(message,key) {
    return CryptoJS.Rabbit.decrypt(message,key).toString(CryptoJS.enc.Utf8)
}
function TripleDES_ET(message,key) {
    return CryptoJS.TripleDES.encrypt(message,key).toString()
}
function TripleDES_DT(message,key) {
    return CryptoJS.TripleDES.decrypt(message,key).toString(CryptoJS.enc.Utf8)
}
