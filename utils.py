# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 29
__function__ = ''

import json
import execjs
import hashlib
import collections
from zlib import crc32
from PyQt5 import QtWidgets,QtGui
from PyQt5.QtWidgets import QMessageBox
from uic.toolkit import Ui_MainWindow
from hashlib import sha1,sha256,sha512,md5
from sha3 import sha3_224,sha3_256,sha3_384,sha3_512

class utils(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(utils, self).__init__()
        self.setupUi(self)
    # 弹出提示窗口
    def tips_box(self, tip_log_leavel, tip_title, tip_message):
        box = QMessageBox(tip_log_leavel, tip_title, tip_message)
        icon = QtGui.QIcon('resources/images/toolkit.ico')
        box.setWindowIcon(icon)
        box.addButton(self.tr("关闭"), QMessageBox.YesRole)
        box.exec_()

    # 计算文件md5
    def get_md5(self,file_path):
        f = open(file_path, 'rb')
        md5_obj = md5()
        while True:
            d = f.read(10240)
            if not d:
                break
            md5_obj.update(d)
        hash_code = md5_obj.hexdigest()
        f.close()
        md5_res = str(hash_code).lower()
        return md5_res
    # 计算文件sha1
    def get_sha1(self,file_path):
        f = open(file_path, 'rb')
        sha1Obj = sha1()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha1Obj.update(d)
        sha1_code = sha1Obj.hexdigest()
        f.close()
        sha1_res = str(sha1_code).lower()
        return sha1_res
    # 计算文件sha256
    def get_sha256(self,file_path):
        f = open(file_path, 'rb')
        sha256Obj = sha256()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha256Obj.update(d)
        sha256_code = sha256Obj.hexdigest()
        f.close()
        sha256_res = str(sha256_code).lower()
        return sha256_res
    # 计算文件sha512
    def get_sha512(self,file_path):
        f = open(file_path, 'rb')
        sha512Obj = sha512()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha512Obj.update(d)
        sha512_code = sha512Obj.hexdigest()
        f.close()
        sha512_res = str(sha512_code).lower()
        return sha512_res

    # 计算文件crc32
    def get_crc32(self,file_path):
        with open(file_path, 'rb') as f:
            return crc32(f.read())

    # 计算文件sha3-224
    def get_sha3_224(self,file_path):
        f = open(file_path, 'rb')
        sha3_224Obj = sha3_224()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha3_224Obj.update(d)
        sha3_224_code = sha3_224Obj.hexdigest()
        f.close()
        sha3_224_res = str(sha3_224_code).lower()
        return sha3_224_res

    # 计算文件sha3-256
    def get_sha3_256(self,file_path):
        f = open(file_path, 'rb')
        sha3_256Obj = sha3_256()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha3_256Obj.update(d)
        sha3_256_code = sha3_256Obj.hexdigest()
        f.close()
        sha3_256_res = str(sha3_256_code).lower()
        return sha3_256_res

    # 计算文件sha3-384
    def get_sha3_384(self,file_path):
        f = open(file_path, 'rb')
        sha3_384Obj = sha3_384()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha3_384Obj.update(d)
        sha3_384_code = sha3_384Obj.hexdigest()
        f.close()
        sha3_384_res = str(sha3_384_code).lower()
        return sha3_384_res

    # 计算文件sha3-512
    def get_sha3_512(self,file_path):
        sha3_512Obj = sha3_512()
        f = open(file_path, 'rb')
        while True:
            d = f.read(10240)
            if not d:
                break
            sha3_512Obj.update(d)
        sha3_512_code = sha3_512Obj.hexdigest()
        print(sha3_512_code)
        f.close()
        sha3_512_res = str(sha3_512_code).lower()
        return sha3_512_res

    def get_ripemd160(self,file_path):
        f = open(file_path, 'rb')
        sha256Obj = sha256()
        while True:
            d = f.read(10240)
            if not d:
                break
            sha256Obj.update(d)
        sha256_code = sha256Obj.hexdigest()
        f.close()
        sha256_res = str(sha256_code).lower()
        obj = hashlib.new('ripemd160', sha256_res.encode('utf-8'))
        return obj.hexdigest()

    # 读取json文件并返回列表
    def read_json_file(self,json_file_path):
        res_list = []
        f = open(json_file_path, encoding='utf-8') # 设置以utf-8解码模式读取文件，encoding参数必须设置，否则默认以gbk模式读取文件，当文件中包含中文时，会报错
        res_dict = json.loads(f.read(),object_pairs_hook=collections.OrderedDict)
        for key,value in zip(res_dict.keys(),res_dict.values()):
            tmp_dict = {
                key:value
            }
            res_list.append(tmp_dict)
        return res_list

    #获取js
    def get_js(self):
        f = open("crypto.js", 'r', encoding='utf-8')  # 打开JS文件
        line = f.readline()
        htmlstr = ''
        while line:
            htmlstr = htmlstr + line
            line = f.readline()
        return htmlstr

    #通过传入算法名称，加密数据和加密密钥进行加密
    def encrypt_decrypt(self,algorithm_name,data,key=None):
        jsstr = utils.get_js(self)
        ctx = execjs.compile(jsstr)
        res = ctx.call(algorithm_name, data, key)
        return res  # 调用js方法  第一个参数是JS的方法名，后面的data和key是js方法的参数

    # 人性化显示字节大小
    def bytes_conversion(self,number):
        symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
        prefix = dict()
        for i, s in enumerate(symbols):
            prefix[s] = 1 << (i + 1) * 10
        for s in reversed(symbols):
            if int(number) >= prefix[s]:
                value = float(number) / prefix[s]
                return '%.2f%s' % (value, s)
        return "%sB" % number

    # 获取当前用户名称
    def  get_win_username(self):
        import getpass
        return getpass.getuser()


