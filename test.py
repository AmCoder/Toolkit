# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 28
__function__ = ''

import requests
import time
import execjs
from uuid import uuid4
from requests_toolbelt.multipart.encoder import MultipartEncoder


header = {
    'Content-type': 'application/octet-stream'
}

boundary = uuid4().hex
appkey = 'oeijjwwm3g5o9bxbw9h84ez24fnxrtnr'
app_secret = '239jqab1sdtzdk300ppn35zdu8es0p8k'
req_time = str(int(time.time()))
ip = "192.168.5.59"
port = 8888

# 生成请求秘钥和刷新秘钥
def make_reqSecret():
    sign = md5(appkey+app_secret+req_time)
    data = {
        'appKey':appkey,
        'sign':sign,
        'reqTime':req_time
    }
    req_res = requests.get('http://%s:%s/risen-app-cloud/public/risen/cloud/fbi/getReqSecret' % (ip,port),
                           headers=header, timeout=120, verify=False, params=data)
    print(req_res.status_code)
    print(req_res.text)

#通过刷新秘钥和appkey获取请求秘钥
def refresh_Secret(app_refresh_secret):
    sign = md5(appkey + app_refresh_secret + req_time)
    data = {
        'appKey':appkey,
        'sign':sign,
        'reqTime': req_time
    }
    req_res = requests.get('http://%s:%s/risen-app-cloud/public/risen/cloud/fbi/updateReqSecret' % (ip,port),
                           headers=header, timeout=120, verify=False, params=data
                           )
    print('appkey:' + appkey)
    print('sign:' + sign)
    print('req_time:' + req_time)
    print(req_res.status_code)
    print(req_res.text)

# 下载文件
def download_file(app_reqSecret,file_path):
    sign = md5(appkey + app_reqSecret + req_time)
    data = {
        'sign':sign,
        'appKey':appkey,
        'reqTime': req_time,
        'filePath':file_path
    }
    req_res = requests.get('http://%s:%s/risen-app-cloud/public/risen/cloud/fbi/sdkFileDownload' % (ip,port),
                                 headers=header, timeout=120, verify=False, params=data)
    print(req_res.status_code)
    with open(r'C:\Users\THINKPAD\Desktop\b.png','wb') as f:
         f.write(req_res.content)
# 上传文件
def upload_file(app_reqSecret):
    sign = md5(appkey + app_reqSecret + req_time)
    f = {'fileFlow': open(r'C:\Users\THINKPAD\Desktop\test.jpg', 'rb')}
    data = {
        'appKey': appkey,
        'sign': sign,
        'reqTime': req_time
    }

    req_res = requests.post('http://%s:%s/risen-app-cloud/public/risen/cloud/fti/fileUpload' % (ip,port),
                           timeout=120, verify=False, data=data, files = f)
    print(req_res.status_code)
    print(req_res.text)

#获取js
def get_js():
    f = open("crypto.js", 'r', encoding='utf-8')  # 打开JS文件
    line = f.readline()
    htmlstr = ''
    while line:
        htmlstr = htmlstr + line
        line = f.readline()
    return htmlstr

def md5(data,key=None):
    jsstr = get_js()
    ctx = execjs.compile(jsstr)
    res = ctx.call('MD5', data, key)
    return res


if __name__ == '__main__':
    #make_reqSecret()
    #print(req_time)
    reqSecret = '0fqn9nauf575018fmziun9f8ipsc6p16'
    upload_file(reqSecret)
    #import base64
    #file_path = str(base64.b64encode('bucket=risen_bucket&dir=wxy_dir&filename=vmware-1.log'.encode('utf-8')),'utf-8')
    #print(file_path)
    #print(str(base64.b64decode('YnVja2V0PXJpc2VuX2J1Y2tldCZkaXI9cmlzZW5fZGlyJmZpbGVuYW1lPTIudm1kaw=='.encode('utf-8')),'utf-8'))

    #file_path = 'YnVja2V0TmFtZT0qYnVja2V0JmRpck5hbWU9KmJ1Y2tldC8qZGlyJmZpbGVOYW1lPXRlc3RtZHQuanBn'
    #print('-----------------------------------------------------------------------------------------------------')
    #refreshSecret = 'r3dip3r0wlwac28b3xmaq0pbg7fls237'
    #refresh_Secret(refreshSecret)
    #download_file(req_secret,file_path)