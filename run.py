# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 26
__function__ = ''

import os
import sys
from uic import markdown
from uic import unit_length
from uic import format_css
from uic import format_json
from uic import refer_ascii
from uic import qrcode
from uic import pinyin
from uic import local_monitor
from uic import format_code_change
from uic import algorithm_file_hash
from uic import algorithm_base64_str
from uic import algorithm_base64_image
from uic import algorithm_single_encrypt
from uic import algorithm_twoway_encrypt_decrypt
from uic import refer_http_content_type
from uic import refer_http_status_code
from PyQt5 import QtWidgets,QtGui,QtCore
from PyQt5.QtWidgets import QMessageBox,QLabel
from uic.toolkit import Ui_MainWindow

class Run(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(Run, self).__init__()
        self.setupUi(self)
        self.status = self.statusBar()
        # 状态栏部件添加状态信息以及设置时间(毫秒)
        self.status.showMessage("本工具目前处于内侧阶段，有问题或者BUG欢迎提出！QQ：xxxxx")

    # UI中设置的槽的名称必须与下面的函数名称一致，才可以被调用
    # json格式化
    def json_format(self):
        format_json.format_json.format_json_fun(self)
    # css格式化
    def css_format(self):
        format_css.format_css.format_css_fun(self)
    # 编码转换
    def native2unicode(self):
        format_code_change.format_code_change.native2unicode_fun(self)
    def unicode2native(self):
        format_code_change.format_code_change.unicode2native_fun(self)
    def native2utf8(self):
        format_code_change.format_code_change.native2utf8_fun(self)
    def utf82native(self):
        format_code_change.format_code_change.utf82native_fun(self)
    def native2ascii(self):
        format_code_change.format_code_change.native2ascii_fun(self)
    def ascii2native(self):
        format_code_change.format_code_change.ascii2native_fun(self)
    def native2url(self):
        format_code_change.format_code_change.native2url_fun(self)
    def url2native(self):
        format_code_change.format_code_change.url2native_fun(self)
    # 图片转base64
    def base64_encrypt_image(self):
        algorithm_base64_image.algorithm_base64_image.base64_encrypt_image_fun(self)
    # base64转图片
    def base64_decrypt_image(self):
        algorithm_base64_image.algorithm_base64_image.base64_decrypt_image_fun(self)
    # 下载base64转换后的图片
    def base64_download_image(self):
        algorithm_base64_image.algorithm_base64_image.base64_download_image_fun(self)
    # base64对字符串进行编码
    def base64_encrypt_str(self):
        algorithm_base64_str.algorithm_base64_str.base64_encrypt_str_fun(self)
    # base64对字符串进行解码
    def base64_decrypt_str(self):
        algorithm_base64_str.algorithm_base64_str.base64_decrypt_str_fun(self)
    # 单向加密
    def hash_str_sha1_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_sha1_encrypt_fun(self)
    def hash_str_sha224_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_sha224_encrypt_fun(self)
    def hash_str_sha256_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_sha256_encrypt_fun(self)
    def hash_str_sha384_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_sha384_encrypt_fun(self)
    def hash_str_sha512_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_sha512_encrypt_fun(self)
    def hash_str_md5_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_md5_encrypt_fun(self)
    def hash_str_hmacsha1_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacsha1_encrypt_fun(self)
    def hash_str_hmacsha224_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacsha224_encrypt_fun(self)
    def hash_str_hmacsha256_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacsha256_encrypt_fun(self)
    def hash_str_hmacsha384_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacsha384_encrypt_fun(self)
    def hash_str_hmacsha512_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacsha512_encrypt_fun(self)
    def hash_str_hmacmd5_encrypt(self):
        algorithm_single_encrypt.algorithm_single_encrypt.hash_str_hmacmd5_encrypt_fun(self)
    # 双向加密
    def encrypt(self):
        algorithm_twoway_encrypt_decrypt.algorithm_twoway_encrypt_decrypt.encrypt_fun(self)
    def decrypt(self):
        algorithm_twoway_encrypt_decrypt.algorithm_twoway_encrypt_decrypt.decrypt_fun(self)
    # 计算文件hash
    def file_hash(self):
        algorithm_file_hash.algorithm_file_hash.file_hash_fun(self)
    def file_hash_choose_all(self):
        algorithm_file_hash.algorithm_file_hash.file_hash_choose_all_fun(self)
    # http content-type表
    def show_http_content_type(self):
        refer_http_content_type.refer_http_content_type.show_http_content_type_fun(self)
    # http status 表
    def show_http_status_code(self):
        refer_http_status_code.refer_http_status_code.show_http_status_code_fun(self)
    # ascii 表
    def show_ascii(self):
        refer_ascii.ascii.show_ascii_fun(self)
    # Markdown编辑器
    def markdown_show(self):
        markdown.markdown.markdown_show_fun(self)
    def markdown_download(self):
        markdown.markdown.markdown_download_fun(self)
    def markdown_html_download(self):
        markdown.markdown.markdown_html_download_fun(self)
    # 本地监控
    def show_local_sys_info(self):
        local_monitor.local_monitor.show_local_sys_info_fun(self)
    def show_local_sys_disk_info(self):
        local_monitor.local_monitor.show_local_sys_disk_info_fun(self)
    def show_local_sys_mem_info(self):
        local_monitor.local_monitor.show_local_sys_mem_info_fun(self)
    # 二维码生成器
    def qr_make(self):
        qrcode.qrcode.qr_make_fun(self)
    def qr_download(self):
        qrcode.qrcode.qr_download_fun(self)
    def qr_logo_upload(self):
        qrcode.qrcode.qr_logo_upload_fun(self)
    def qr_logo_delete(self):
        qrcode.qrcode.qr_logo_delete_fun(self)
    def show_qr_combobox(self):
        qrcode.qrcode.show_qr_combobox_fun(self)
    # 长度换算
    def unit_length_num_change(self):
        unit_length.unit_length.unit_length_num_change_fun(self)
    def show_unit_length(self):
        unit_length.unit_length.show_unit_length_fun(self)

    # 识别汉字拼音
    def show_pinyin(self):
        pinyin.pinyin.show_pinyin_fun(self)

    # 关闭窗口触发以下事件
    def closeEvent(self, event):
        reply = QMessageBox.question(self, '关闭提示', '你确定要退出HelpTool吗?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            # 退出时删除二维码logo图片
            qr_tmp_logo_path = r'resources/others/qr_logo_tmp.png'
            if os.path.exists(qr_tmp_logo_path):
                os.remove(qr_tmp_logo_path)
            # 退出时删除二维码图片
            qr_tmp_path = r'resources/others/qr_code.png'
            if os.path.exists(qr_tmp_path):
                os.remove(qr_tmp_path)
            # 退出时删除base64转换后的图片
            base64_tmp_type_list = ['.jpg', '.png', '.jpeg', '.gif', '.bmp', '.ico']
            for type in base64_tmp_type_list:
                base64_tmp_path = r'resources/others/base64_encrypt_tmp%s' % type
                if os.path.exists(base64_tmp_path):
                    os.remove(base64_tmp_path)
            event.accept() # 接受关闭事件
        else:
            event.ignore() # 忽略关闭事件

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    # label = QtWidgets.QLabel('')
    # label.setFixedSize(400, 325)
    # label.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool)
    # label.setAttribute(QtCore.Qt.WA_TranslucentBackground)
    # movie = QtGui.QMovie("resources/images/loading.gif")
    # label.setMovie(movie)
    # label.show()
    # movie.setSpeed(250)
    # movie.start()

    app.processEvents()

    myshow = Run()

    myshow.show_http_content_type()
    myshow.show_http_status_code()
    myshow.show_ascii()
    myshow.show_qr_combobox()
    myshow.show_unit_length()
    #myshow.qr_choose_tab_widget.setStyleSheet("QPushButton{background: transparent;}")
    myshow.show()

    #splash = QtWidgets.QSplashScreen(QtGui.QPixmap("resources/images/loading.gif"))
    #splash.show()
    # 定义字体格式
    # font = QtGui.QFont()
    # font.setPointSize(16)
    # font.setBold(True)
    # font.setWeight(75)
    # splash.setFont(font)
    # splash.showMessage("正在启动程序....", QtCore.Qt.AlignBottom, QtCore.Qt.red)
    #time.sleep(6)
    #splash.close()

    sys.exit(app.exec_())
