# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 6
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
from PyQt5.QtWidgets import QMessageBox
import utils
import webbrowser

class algorithm_single_encrypt(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(algorithm_single_encrypt, self).__init__()
        self.setupUi(self)
    def hash_str_sha1_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='SHA1', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')


    def hash_str_sha224_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='SHA224', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')

    def hash_str_sha256_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='SHA256', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')

    def hash_str_sha384_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='SHA384', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')

    def hash_str_sha512_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='SHA512', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')

    def hash_str_md5_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        if len(input_str) != 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='MD5', data=input_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密内容！')
    def hash_str_hmacsha1_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacSHA1', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def hash_str_hmacsha224_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacSHA224', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def hash_str_hmacsha256_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacSHA256', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def hash_str_hmacsha384_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacSHA384', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def hash_str_hmacsha512_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacSHA512', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def hash_str_hmacmd5_encrypt_fun(self):
        self.hash_encrypt_output_text.clear()
        input_str = self.hash_str_input_text.toPlainText()
        key_str = self.hash_key_input_text.text()
        if len(key_str) != 0 and len(input_str)!= 0:
            try:
                sha1_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='HmacMD5', data=input_str, key=key_str)
                self.hash_encrypt_output_text.append(str(sha1_str_res))
            except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')
