# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 29
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
from PyQt5.QtWidgets import QMessageBox
import utils


class format_css(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(format_css, self).__init__()
        self.setupUi(self)

    #css代码格式化
    def format_css_fun(self):
        self.css_output_text.clear()
        input_str = self.css_input_text.toPlainText()
        try:
            if len(input_str.strip()) != 0:
                self.css_output_text.append(input_str.replace("{", "{\n    ").replace("}", "\n}\n\n").replace(";", ";\n    "))
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, "错误格式提示", '请输入需要格式化的内容!')
        except Exception as e:
            utils.utils.tips_box(self, QMessageBox.Warning, "错误格式提示", str(e))