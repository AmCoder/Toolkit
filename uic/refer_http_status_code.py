# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 3
__function__ = ''

from PyQt5 import QtWidgets
import utils
from PyQt5.QtWidgets import QTableWidgetItem,QHeaderView
from uic.toolkit import Ui_MainWindow
class refer_http_status_code(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(refer_http_status_code, self).__init__()
        self.setupUi(self)

    def show_http_status_code_fun(self):
        data_list = utils.utils.read_json_file(self, 'resources/data_files/http_status_code.json')
        self.http_status_code_tablewidget.setRowCount(len(data_list))
        self.http_status_code_tablewidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.http_status_code_tablewidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        for i in range(0, len(data_list)):
            for key, value in zip(data_list[i].keys(), data_list[i].values()):
                self.http_status_code_tablewidget.setItem(i, 0, QTableWidgetItem(key))
                self.http_status_code_tablewidget.setItem(i, 1, QTableWidgetItem(value))
                self.http_status_code_tablewidget.setRowHeight(i,100)