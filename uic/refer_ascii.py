# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 4
__function__ = ''

from PyQt5 import QtWidgets
import utils
from PyQt5.QtWidgets import QTableWidgetItem,QHeaderView
from uic.toolkit import Ui_MainWindow
class ascii(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(ascii, self).__init__()
        self.setupUi(self)

    def show_ascii_fun(self):
        data_list = utils.utils.read_json_file(self, 'resources/data_files/ascii.json')
        self.ascii_tablewidget.setRowCount(32)
        self.ascii_tablewidget.setColumnCount(8)
        self.ascii_tablewidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ascii_tablewidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        count = 0  # 计数json的长度
        for i in range(0, int(len(data_list) / 4)):  # 控制行 128/4 = 32行
                for j in range(0,8,2): # 控制列 4列
                    self.ascii_tablewidget.setItem(i, j, QTableWidgetItem(str(count)))
                    self.ascii_tablewidget.setItem(i, j + 1, QTableWidgetItem(str(data_list[count][str(count)])))
                    count = count + 1