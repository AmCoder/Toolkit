# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 6
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
from PyQt5.QtWidgets import QMessageBox
import utils
import webbrowser

class algorithm_twoway_encrypt_decrypt(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(algorithm_twoway_encrypt_decrypt, self).__init__()
        self.setupUi(self)
    def encrypt_fun(self):
        self.decrypt_input_text.clear()
        if self.aes_check_box.isChecked():
            input_str = self.encrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(input_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='AES_ET', data=input_str, key=key_str)
                    self.decrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

        if self.des_check_box.isChecked():
            input_str = self.encrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(input_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='DES_ET', data=input_str, key=key_str)
                    self.decrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

        if self.rc4_check_box.isChecked():
            input_str = self.encrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(input_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='RC4_ET', data=input_str, key=key_str)
                    self.decrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

        if self.rabbit_check_box.isChecked():
            self.tripledes_check_box.setChecked(False)
            input_str = self.encrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(input_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='Rabbit_ET', data=input_str, key=key_str)
                    self.decrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

        if self.tripledes_check_box.isChecked():
            input_str = self.encrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(input_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='TripleDES_ET', data=input_str, key=key_str)
                    self.decrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入加密密钥或加密内容！')

    def decrypt_fun(self):
        self.encrypt_input_text.clear()
        if self.aes_check_box.isChecked():
            out_str = self.decrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(out_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='AES_DT', data=out_str, key=key_str)
                    self.encrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解密密钥或解密内容！')

        if self.des_check_box.isChecked():
            out_str = self.decrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(out_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='DES_DT', data=out_str, key=key_str)
                    self.encrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解密密钥或解密内容！')

        if self.rc4_check_box.isChecked():
            out_str = self.decrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(out_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='RC4_DT', data=out_str, key=key_str)
                    self.encrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解密密钥或解密内容！')

        if self.rabbit_check_box.isChecked():
            out_str = self.decrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(out_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='Rabbit_DT', data=out_str, key=key_str)
                    self.encrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解密密钥或解密内容！')

        if self.tripledes_check_box.isChecked():
            out_str = self.decrypt_input_text.toPlainText()
            key_str = self.key_line_text.text()
            if len(out_str) != 0 and len(key_str) != 0:
                try:
                    aes_str_res = utils.utils.encrypt_decrypt(self, algorithm_name='TripleDES_DT', data=out_str, key=key_str)
                    self.encrypt_input_text.appendPlainText(str(aes_str_res))
                except Exception as e:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '%s\n可能缺少node.js环境，关闭后自动跳转下载页面，安装完成后请重启HelpTool！' % str(e))
                    webbrowser.open('https://nodejs.org/en/', new=0, autoraise=True)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解密密钥或解密内容！')