# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 7
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
import wmi
import json
import psutil
import platform
import utils


class local_monitor(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(local_monitor, self).__init__()
        self.setupUi(self)

    def show_local_sys_info_fun(self):
        self.local_sys_output_text.clear()
        if self.local_sys_monitor_check_box.isChecked():
            res_dict = {}
            info = wmi.WMI()
            logic_cpu_count = psutil.cpu_count()
            physical_cpu_count = psutil.cpu_count(logical=False)
            processer = int(logic_cpu_count) * int(physical_cpu_count)
            for cpu_info in info.Win32_Processor():
                cpu_model = cpu_info.Name.strip()
                res_dict[u'CPU型号'] = cpu_model  # CPU型号
            res_dict['物理CPU个数'] = int(physical_cpu_count)  # 物理CPU个数
            res_dict['逻辑CPU个数'] = int(logic_cpu_count)  # 逻辑CPU个数
            res_dict['总核数'] = int(processer)  # 总核数=总线程
            res_dict['CPU使用百分比'] = "%0.2f" % psutil.cpu_percent(interval=1) + "%"
            res_dict['操作系统类型'] = platform.platform()  # 操作系统名称和类型
            res_dict['操作系统版本'] = platform.version()  # 操作系统版本
            res_dict['操作系统位数'] = platform.architecture()[0]  # 操作系统位数
            res_dict['计算机类型'] = platform.machine()  # 计算机类型
            res_dict['计算机网络名称'] = platform.node()  # 计算机网络名称
            res_json = json.dumps(res_dict, sort_keys=True, indent=2, separators=(',', ':'),ensure_ascii=False)
            self.local_sys_output_text.append(res_json)

    def show_local_sys_disk_info_fun(self):
        self.local_sys_disk_output_text.clear()
        if self.local_sys_disk_check_box.isChecked():
            res_list = []
            disk_name_list = []
            disk_type_list = []
            disk = psutil.disk_partitions()
            for dk in disk:
                if str(dk[2]) != '':
                    disk_name_list.append(dk[1])
                    disk_type_list.append(dk[2])
            for i, j in zip(disk_name_list, disk_type_list):
                disk_dict = {
                    '磁盘名称': i,
                    '磁盘类型': j,
                    '磁盘总大小': utils.utils.bytes_conversion(self, psutil.disk_usage(i).total),
                    '磁盘使用大小': utils.utils.bytes_conversion(self, psutil.disk_usage(i).used),
                    '磁盘剩余大小': utils.utils.bytes_conversion(self, psutil.disk_usage(i).free),
                    '磁盘使用百分比':"%s%%" % str(psutil.disk_usage(i).percent)
                }
                res_list.append(disk_dict)
                res_json = json.dumps(res_list, sort_keys=True, indent=2, separators=(',', ':'),ensure_ascii=False)
                self.local_sys_disk_output_text.append(res_json)

    def show_local_sys_mem_info_fun(self):
        self.local_sys_mem_output_text.clear()
        if self.local_sys_mem_check_box.isChecked():
                res_dict = {}
                mem = psutil.virtual_memory()
                res_dict['总内存大小'] = utils.utils.bytes_conversion(self, mem.total)
                res_dict['剩余内存大小'] = utils.utils.bytes_conversion(self, mem.free)
                res_dict['使用内存大小'] = utils.utils.bytes_conversion(self, mem.used)
                res_dict['使用内存百分比'] = "%s%%" % str(mem.percent)
                res_json = json.dumps(res_dict, sort_keys=True, indent=4, separators=(',', ':'),ensure_ascii=False)
                self.local_sys_mem_output_text.append(res_json)
