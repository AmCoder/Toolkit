# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 7
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
import re

class format_code_change(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(format_code_change, self).__init__()
        self.setupUi(self)

    def native2unicode_fun(self):
        pass
    def unicode2native_fun(self):
        pass
    def native2utf8_fun(self):
        pass
    def utf82native_fun(self):
        pass
    def native2ascii_fun(self):
        self.native2ascii_output_text.clear()
        input_str = self.native2ascii_input_text.toPlainText()
        if self.native_ascii_check_box.isChecked():
            self.native2ascii_output_text.appendPlainText(format_code_change.charToUnic(self,input_str))
        else:
            output_str = str(input_str.encode('unicode-escape'), encoding='utf-8')
            self.native2ascii_output_text.appendPlainText(output_str)
    def ascii2native_fun(self):
        pass
    def native2url_fun(self):
        pass
    def url2native_fun(self):
        pass

    def charToUnic(self,input_str):
        hanzi_regex = re.compile(r'[\u4e00-\u9fa5]+')
        res_list = []
        for i,char in enumerate(input_str):
            hanzi_list = hanzi_regex.findall(char)
            if len(hanzi_list) !=0:
                hanzi_dict = {
                    i : str(''.join(hanzi_list).encode('unicode-escape'), encoding='utf-8')
                }
                res_list.append(hanzi_dict)
            else:
                tmp_ch = hex(ord(char))[2:]
                char_res = "0" * (4 - len(tmp_ch)) + tmp_ch
                zifu_dict = {
                    i: '\\u%s' % char_res
                }
                res_list.append(zifu_dict)
        res_str_list = []
        for ele in res_list:
            for value in ele.values():
                res_str_list.append(value)
        return ''.join(res_str_list)

