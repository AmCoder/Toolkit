# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 17
__function__ = ''


import pypinyin
from PyQt5 import QtWidgets,QtGui
from PyQt5.QtWidgets import QFileDialog,QMessageBox
from uic.toolkit import Ui_MainWindow

class pinyin(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(pinyin, self).__init__()
        self.setupUi(self)

    def show_pinyin_fun(self):
        self.pinyin_output_text.clear()
        input_str = self.pinyin_input_text.toPlainText()
        res = pinyin.hp(self, input_str)
        self.pinyin_output_text.append(res)

    def hp(self,word):
        s = ''
        for i in pypinyin.pinyin(word):
            s = s + ''.join(i) + " "
        return s

