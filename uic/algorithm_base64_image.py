# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 2
__function__ = ''


import re
import os
import base64
import utils
from PyQt5 import QtWidgets,QtGui
from PyQt5.QtWidgets import QFileDialog,QMessageBox
from uic.toolkit import Ui_MainWindow

class algorithm_base64_image(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(algorithm_base64_image, self).__init__()
        self.setupUi(self)

    def base64_encrypt_image_fun(self):
        win_username = utils.utils.get_win_username(self)
        self.base64_output_image_text.clear()
        # 设置读取文件弹框
        image_path, image_type = QFileDialog.getOpenFileName(self, "选择文件", r"C:\Users\%s\Desktop" % win_username , "*.jpg;;*.png;;*.jpeg;;*.gif;;*.bmp;;*.ico")
        try:
            image = QtGui.QPixmap(image_path).scaled(self.base64_show_image_label.width() - 15,
                                                     self.base64_show_image_label.height() - 15)
            self.base64_show_image_label.setPixmap(image)
            if len(image_path) != 0:
                with open(image_path,'rb') as fin:
                    image_data = fin.read()
                    base64_data = str(base64.b64encode(image_data),'utf-8')
                    self.base64_output_image_text.insertPlainText('data:image/%s;base64,%s' % (str(image_type).split('.')[1],base64_data))
        except Exception as e:
            print("base64加密图片失败=====>"+str(e))
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '加密失败！')

    def base64_decrypt_image_fun(self):
        try:
            image_type = re.match(r"data:image/(.*);base64,", self.base64_output_image_text.toPlainText())
            if image_type:
                image_data = re.match(r"data:image/%s;base64,(.*)" % image_type.group(1), self.base64_output_image_text.toPlainText()).group(1)
                ori_image_data = base64.b64decode(image_data)
                image_path = 'resources/others/base64_encrypt_tmp.%s' % image_type.group(1)
                fout = open(image_path, 'wb')
                fout.write(ori_image_data)
                fout.close()
                image = QtGui.QPixmap(image_path).scaled(self.base64_show_image_label.width() - 15,
                                                         self.base64_show_image_label.height() - 15)
                self.base64_show_image_label.setPixmap(image)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '解密失败！')
        except Exception as e:
            print("base64解密图片失败=====>" + str(e))
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '解密失败！')

    def base64_download_image_fun(self):
        win_username = utils.utils.get_win_username(self)
        image_type = re.match(r"data:image/(.*);base64,", self.base64_output_image_text.toPlainText())
        try:
            if image_type:
                image_path = 'resources/others/base64_encrypt_tmp.%s' % image_type.group(1)
                image_download_path, image_type = QFileDialog.getSaveFileName(self, "保存文件", r"C:\Users\%s\Downloads\save" % win_username, "*.jpg;;*.png;;*.jpeg;;*.gif;;*.bmp;;*.ico")
                with open(image_download_path,'wb') as fout:
                    with open(image_path,'rb') as fin:
                        fout.write(fin.read())
                    fin.close()
                fout.close()
                if os.path.exists(image_download_path):
                    utils.utils.tips_box(self, QMessageBox.Warning, '下载提示', '下载成功！')
                else:
                    utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '下载失败！')
        except Exception as  e:
            pass
