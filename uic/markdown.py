# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 7
__function__ = ''

import markdown as md
import utils
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog,QMessageBox
from uic.toolkit import Ui_MainWindow
import getpass

class markdown(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(markdown, self).__init__()
        self.setupUi(self)

    def markdown_show_fun(self):
        self.markdown_output_text.clear()
        if self.markdown_show_check_box.isChecked():
            input_str = self.markdown_input_text.toPlainText()
            output_str = md.markdown(input_str,extensions=[
                'markdown.extensions.codehilite',
                'markdown.extensions.extra',
                'markdown.extensions.tables',
                'markdown.extensions.toc'
            ])
            self.markdown_output_text.append(output_str)

    def markdown_download_fun(self):
        win_username = getpass.getuser()
        try:
            file_path, file_type = QFileDialog.getSaveFileName(self, "保存文件", r"C:\Users\%s\Desktop\save" % win_username,"*.md")
            with open(file_path, 'w') as fout:
                fout.write(self.markdown_input_text.toPlainText())
            fout.close()
            utils.utils.tips_box(self, QMessageBox.Warning, '下载提示', '下载成功！')
        except Exception as e:
            pass
    def markdown_html_download_fun(self):
        html = '''
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
	%s
	</body>
</html>
        '''
        win_username = getpass.getuser()
        try:
            file_path, file_type = QFileDialog.getSaveFileName(self, "保存文件", r"C:\Users\%s\Desktop\save" % win_username,"*.html")
            input_html = md.markdown(self.markdown_input_text.toPlainText())
            with open(file_path, 'w') as fout:
                fout.write(html % input_html)
            fout.close()
            utils.utils.tips_box(self, QMessageBox.Warning, '下载提示', '下载成功！')
        except Exception as e:
            pass
