# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 2
__function__ = ''

from PyQt5 import QtWidgets
from uic.toolkit import Ui_MainWindow
from PyQt5.QtWidgets import QFileDialog
import utils
from datetime import datetime

class algorithm_file_hash(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(algorithm_file_hash, self).__init__()
        self.setupUi(self)

    def file_hash_fun(self):
        win_username = utils.utils.get_win_username(self)
        file_path, file_type = QFileDialog.getOpenFileName(self, "选择文件", r"C:\Users\%s\Desktop" % win_username,"*.*")
        try:
            st_time = datetime.now()
            if self.crc_32_check_box.isChecked():
                self.crc32_file_output_text.clear()
                #self.crc32_file_output_text.append(str(utils.utils.get_crc32(self, file_path)))
                self.crc32_file_output_text.append("暂时无法使用")
            if self.md5_hash_check_box.isChecked():
                self.md5_file_output_text.clear()
                self.md5_file_output_text.append(str(utils.utils.get_md5(self, file_path)))
            if self.sha1_hash_check_box.isChecked():
                self.sha1_hash_file_output_text.clear()
                self.sha1_hash_file_output_text.append(str(utils.utils.get_sha1(self, file_path)))
            if self.sha512_check_box.isChecked():
                self.sha512_file_output_text.clear()
                self.sha512_file_output_text.append(str(utils.utils.get_sha512(self, file_path)))
            if self.sha256_check_box.isChecked():
                self.sha256_file_output_text.clear()
                self.sha256_file_output_text.append(str(utils.utils.get_sha256(self, file_path)))
            if self.sha3_224_check_box.isChecked():
                self.sha3_224_file_output_text.clear()
                #self.sha3_224_file_output_text.append(str(utils.utils.get_sha3_224(self, file_path)))、
                self.sha3_224_file_output_text.append("暂时无法使用")
            if self.sha3_256_check_box.isChecked():
                self.sha3_256_file_output_text.clear()
                #self.sha3_256_file_output_text.append(str(utils.utils.get_sha3_256(self, file_path)))
                self.sha3_256_file_output_text.append("暂时无法使用")
            if self.sha3_384_check_box.isChecked():
                self.sha3_384_file_output_text.clear()
                #self.sha3_384_file_output_text.append(str(utils.utils.get_sha3_384(self, file_path)))
                self.sha3_384_file_output_text.append("暂时无法使用")
            if self.sha3_512_check_box.isChecked():
                self.sha3_512_file_output_text.clear()
                #self.sha3_512_file_output_text.append(str(utils.utils.get_sha3_512(self, file_path)))
                self.sha3_512_file_output_text.append("暂时无法使用")
            if self.ripemd_160_check_box.isChecked():
                self.ripemd_160_file_output_text.clear()
                #self.ripemd_160_file_output_text.append(str(utils.utils.get_ripemd160(self, file_path)))
                self.ripemd_160_file_output_text.append("暂时无法使用")
            ed_time = datetime.now()
            use_time = (ed_time - st_time).seconds
            self.file_info_label.setText('文件路径：%s  耗时：%s秒' % (file_path, use_time))
        except Exception as e:
            print(e)
    def file_hash_choose_all_fun(self):
        self.crc_32_check_box.setChecked(True)
        self.md5_hash_check_box.setChecked(True)
        self.sha1_hash_check_box.setChecked(True)
        self.ripemd_160_check_box.setChecked(True)
        self.sha256_check_box.setChecked(True)
        self.sha512_check_box.setChecked(True)
        self.sha3_384_check_box.setChecked(True)
        self.sha3_256_check_box.setChecked(True)
        self.sha3_512_check_box.setChecked(True)
        self.sha3_224_check_box.setChecked(True)





