# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 29
__function__ = ''

import json
import utils
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
from uic.toolkit import Ui_MainWindow

class format_json(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(format_json, self).__init__()
        self.setupUi(self)

    # Json格式化
    def format_json_fun(self):

        self.json_output_text.clear()
        input_str = self.json_input_text.toPlainText()
        try:
            if len(input_str.strip()) != 0:
                str_2_dict = json.loads(input_str)
                json_format_str = json.dumps(str_2_dict, indent=4, separators=(',', ':'),ensure_ascii=False,sort_keys=True)
                self.json_output_text.append(json_format_str)
            else:
                utils.utils.tips_box(self, QMessageBox.Warning, "错误格式提示", '请输入需要格式化的内容!')
        except Exception as e:
                utils.utils.tips_box(self, QMessageBox.Warning, "错误格式提示", str(e))

