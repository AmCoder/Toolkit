# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2018 / 12 / 29
__function__ = ''

from PyQt5 import QtWidgets
import utils
from PyQt5.QtWidgets import QTableWidgetItem,QHeaderView
from uic.toolkit import Ui_MainWindow
class refer_http_content_type(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(refer_http_content_type, self).__init__()
        self.setupUi(self)

    def show_http_content_type_fun(self):
        self.http_content_type_tablewidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.http_content_type_tablewidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        data_list = utils.utils.read_json_file(self, 'resources/data_files/http_content_type.json')
        self.http_content_type_tablewidget.setRowCount(len(data_list))
        for i in range(0,len(data_list)):
            for key,value in zip(data_list[i].keys(),data_list[i].values()):
                self.http_content_type_tablewidget.setItem(i, 0, QTableWidgetItem(key))
                self.http_content_type_tablewidget.setItem(i, 1, QTableWidgetItem(value))