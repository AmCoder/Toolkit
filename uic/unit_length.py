# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 16
__function__ = ''

from PyQt5 import QtWidgets
import utils
import re
from PyQt5.QtWidgets import QTableWidgetItem,QHeaderView
from uic.toolkit import Ui_MainWindow
class unit_length(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(unit_length, self).__init__()
        self.setupUi(self)
    def unit_length_num_change_fun(self):
        self.mi_output_text.clear()
        self.yingli_output_text.clear()
        self.haomi_output_text.clear()
        self.haili_output_text.clear()
        self.li_output_text.clear()
        self.limi_output_text.clear()
        self.fenmi_output_text.clear()
        self.ma_output_text.clear()
        self.weimi_output_text.clear()
        self.yingxun_output_text.clear()
        self.nami_output_text.clear()
        self.fulong_output_text.clear()
        self.pimi_output_text.clear()
        self.zhang_output_text.clear()
        self.guangnian_output_text.clear()
        self.chi_output_text.clear()
        self.tianwendanwei_output_text.clear()
        self.cun_output_text.clear()
        self.yingcun_output_text.clear()
        self.fen_output_text.clear()
        self.inch_output_text.clear()
        self.li2_output_text.clear()
        self.yingchi_output_text.clear()
        self.hao_output_text.clear()
        input_str = self.length_input_text.text()
        unit_length.unit2km(self,input_str)

    def show_unit_length_fun(self):
        unit_list = ["千米(km)","米(m)","分米(dm)","厘米(cm)","微米(um)","纳米(nm)","皮米(pm)","光年(ly)","天文单位(AU)","英寸(in)","inch(in)","英尺(ft)","码(yd)","英里(mi)","海里(nmi)","英寻(fm)","弗隆(fur)","里","丈","尺","寸","分","厘","毫"]
        for i,value in enumerate(unit_list):
            self.length_combobox.insertItem(i, value)

    def unit2km(self,input_str):
        #input_str 为千米，以下方式都通过转为千米来实现计算
        pattern = re.compile(r'^([1-9]\d*|0)(\.\d*)?$')
        if pattern.match(input_str):
            if '米(m)' in self.length_combobox.currentText(): # 判断combobox是什么单位
                self.mi_length_label.setText('千米(km)') # 将千米设置到里面的label中
                num = float(input_str) / 1000 # 无论是什么单位都统一转为千米
                if  '.' in str(num):
                    unit_length.reduce_float(self,num)  # 如果是小数则调用reduce_float
                    self.mi_output_text.clear() # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.mi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.mi_output_text.clear()
                    self.mi_output_text.append(str(input_str(num)))

            if  '千米(km)' in self.length_combobox.currentText():
                num = float(input_str)
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                else:
                    unit_length.reduce_int(self, num)

            if '分米(dm)' in self.length_combobox.currentText():
                self.fenmi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 10000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.fenmi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fenmi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.fenmi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fenmi_output_text.append(str(int(num)))

            if  '厘米(cm)' in self.length_combobox.currentText():
                self.limi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 100000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.limi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.limi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.limi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.limi_output_text.append(str(int(num)))

            if '微米(um)' in self.length_combobox.currentText():
                self.weimi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 1000000000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.weimi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.weimi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.weimi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.weimi_output_text.append(str(int(num)))

            if '纳米(nm)' in self.length_combobox.currentText():
                self.nami_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 1000000000000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.nami_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.nami_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.nami_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.nami_output_text.append(str(int(num)))

            if '皮米(pm)' in self.length_combobox.currentText():
                self.pimi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 1e+15
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.pimi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.pimi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.pimi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.pimi_output_text.append(str(int(num)))

            if '光年(ly)' in self.length_combobox.currentText():
                self.guangnian_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 1.0570e-13
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.guangnian_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.guangnian_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.guangnian_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.guangnian_output_text.append(str(int(num)))

            if '天文单位(AU)' in self.length_combobox.currentText():
                self.tianwendanwei_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 6.6846e-9
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.tianwendanwei_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.tianwendanwei_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.tianwendanwei_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.tianwendanwei_output_text.append(str(int(num)))

            if '英寸(in)' in self.length_combobox.currentText():
                self.yingcun_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 39370.0787402
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.yingcun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingcun_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.yingcun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingcun_output_text.append(str(int(num)))

            if  'inch(in)' in self.length_combobox.currentText():
                self.inch_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 39370.0787402
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.inch_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.inch_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.inch_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.inch_output_text.append(str(int(num)))

            if  '英尺(ft)' in self.length_combobox.currentText():
                self.yingchi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 3280.839895
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.yingchi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingchi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.yingchi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingchi_output_text.append(str(int(num)))

            if  '码(yd)' in self.length_combobox.currentText():
                self.ma_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 1093.6132983
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.ma_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.ma_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.ma_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.ma_output_text.append(str(int(num)))

            if  '英里(mi)' in self.length_combobox.currentText():
                self.yingli_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 0.6213712
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.yingli_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingli_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.yingli_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingli_output_text.append(str(int(num)))

            if  '海里(nmi)' in self.length_combobox.currentText():
                self.haili_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 0.5399568
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.haili_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.haili_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.haili_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.haili_output_text.append(str(int(num)))

            if  '英寻(fm)' in self.length_combobox.currentText():
                self.yingxun_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 546.8066492
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.yingxun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingxun_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.yingxun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.yingxun_output_text.append(str(int(num)))

            if  '弗隆(fur)' in self.length_combobox.currentText():
                self.fulong_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 4.9709695
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.fulong_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fulong_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.fulong_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fulong_output_text.append(str(int(num)))

            if  '里' == self.length_combobox.currentText():
                self.li_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 2
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.li_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.li_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.li_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.li_output_text.append(str(int(num)))

            if  '丈' == self.length_combobox.currentText():
                self.zhang_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 300
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.zhang_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.zhang_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.zhang_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.zhang_output_text.append(str(int(num)))

            if  '尺' == self.length_combobox.currentText():
                self.chi_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 3000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.chi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.chi_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.chi_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.chi_output_text.append(str(int(num)))

            if  '寸' == self.length_combobox.currentText():
                self.cun_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 30000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.cun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.cun_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.cun_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.cun_output_text.append(str(int(num)))

            if  '分' == self.length_combobox.currentText():
                self.fen_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 300000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.fen_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fen_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.fen_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.fen_output_text.append(str(int(num)))

            if  '厘' == self.length_combobox.currentText():
                self.li2_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 3000000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.li2_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.li2_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.li2_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.li2_output_text.append(str(int(num)))

            if  '毫' == self.length_combobox.currentText():
                self.hao_length_label.setText('千米(km)')  # 将千米设置到里面的label中
                num  = float(input_str)  / 30000000
                if  isinstance(num,float):
                    unit_length.reduce_float(self,num)
                    self.hao_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.hao_output_text.append(str(float(num)))
                else:
                    unit_length.reduce_int(self, num)
                    self.hao_output_text.clear()  # 清除掉跟combobox中的值一样的text,并设置新的给他
                    self.hao_output_text.append(str(int(num)))


    def reduce_float(self,num):
        self.mi_output_text.append(str(float(num) * 1000))
        self.yingli_output_text.append(str(float(num) * 0.6213712))
        self.haomi_output_text.append(str(float(num) * 1000000))
        self.haili_output_text.append(str(float(num) * 0.5399568))
        self.li_output_text.append(str(float(num) * 2))
        self.limi_output_text.append(str(float(num) * 100000))
        self.fenmi_output_text.append(str(float(num) * 10000))
        self.ma_output_text.append(str(float(num) * 1093.6132983))
        self.weimi_output_text.append(str(float(num) * 1000000000))
        self.yingxun_output_text.append(str(float(num) * 546.8066492))
        self.nami_output_text.append(str(float(num) * 1000000000000))
        self.fulong_output_text.append(str(float(num) * 4.9709695))
        self.pimi_output_text.append(str(float(num) * 1e+15))
        self.zhang_output_text.append(str(float(num) * 300))
        self.guangnian_output_text.append(str(float(num) * 1.0570e-13))
        self.chi_output_text.append(str(float(num) * 3000))
        self.tianwendanwei_output_text.append(str(float(num) * 6.6846e-9))
        self.cun_output_text.append(str(float(num) * 30000))
        self.yingcun_output_text.append(str(float(num) * 39370.0787402))
        self.fen_output_text.append(str(float(num) * 300000))
        self.inch_output_text.append(str(float(num) * 39370.0787402))
        self.li2_output_text.append(str(float(num) * 3000000))
        self.yingchi_output_text.append(str(float(num) * 3280.839895))
        self.hao_output_text.append(str(float(num) * 30000000))


    def reduce_int(self,num):
        self.mi_output_text.append(str(int(num) * 1000))
        self.yingli_output_text.append(str(int(num) * 0.6213712))
        self.haomi_output_text.append(str(int(num) * 1000000))
        self.haili_output_text.append(str(int(num) * 0.5399568))
        self.li_output_text.append(str(int(num) * 2))
        self.limi_output_text.append(str(int(num) * 100000))
        self.fenmi_output_text.append(str(int(num) * 10000))
        self.ma_output_text.append(str(int(num) * 1093.6132983))
        self.weimi_output_text.append(str(int(num) * 1000000000))
        self.yingxun_output_text.append(str(int(num) * 546.8066492))
        self.nami_output_text.append(str(int(num) * 1000000000000))
        self.fulong_output_text.append(str(int(num) * 4.9709695))
        self.pimi_output_text.append(str(int(num) * 1e+15))
        self.zhang_output_text.append(str(int(num) * 300))
        self.guangnian_output_text.append(str(int(num) * 1.0570e-13))
        self.chi_output_text.append(str(int(num) * 3000))
        self.tianwendanwei_output_text.append(str(int(num) * 6.6846e-9))
        self.cun_output_text.append(str(int(num) * 30000))
        self.yingcun_output_text.append(str(int(num) * 39370.0787402))
        self.fen_output_text.append(str(int(num) * 300000))
        self.inch_output_text.append(str(int(num) * 39370.0787402))
        self.li2_output_text.append(str(int(num) * 3000000))
        self.yingchi_output_text.append(str(int(num) * 3280.839895))
        self.hao_output_text.append(str(int(num) * 30000000))


