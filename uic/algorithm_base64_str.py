# -*- coding: utf-8 -*-
__author__ = 'ranzechen'
__time__ = 2019 / 1 / 7
__function__ = ''

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
from uic.toolkit import Ui_MainWindow
import utils
import base64 as bs64

class algorithm_base64_str(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(algorithm_base64_str, self).__init__()
        self.setupUi(self)

    def base64_encrypt_str_fun(self):
        self.base64_str_output_text.clear()
        base64_input_str = self.base64_str_input_text.toPlainText()
        if len(base64_input_str) != 0:
            try:
                base64_res = str(bs64.b64encode(base64_input_str.encode('utf-8')), 'utf-8')
                self.base64_str_output_text.appendPlainText(base64_res)
            except Exception as e:
                print("base64编码失败异常信息======>" + str(e))
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', 'BASE64编码失败！')
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入编码内容！')

    def base64_decrypt_str_fun(self):
        self.base64_str_input_text.clear()
        base64_output_str = self.base64_str_output_text.toPlainText()
        if len(base64_output_str) != 0:
            try:
                base64_res = str(bs64.b64decode(base64_output_str.encode('utf-8')), 'utf-8')
                self.base64_str_input_text.appendPlainText(base64_res)
            except Exception as e:
                print("base64解码失败异常信息======>" + str(e))
                utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', 'BASE64解码失败！')
        else:
            utils.utils.tips_box(self, QMessageBox.Warning, '错误提示', '请输入解码内容！')